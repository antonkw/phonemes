package by.bsuir.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.StringUtils;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Phoneme {
    public String toString() {
        String string = StringUtils.EMPTY;

        if (stunned != null && stunned) {
            string += "stunned ";
        }

        if (vowel != null) {
            if (vowel) {
                string += "vowel ";
            } else {
                string += "not vowel ";
            }

        }

        if (AccentType.ACCENT.equals(this.accentType)) {
            string += "accent ";
        }

        if (doubled != null && doubled) {
            string += "doubled ";
        }

        if (soft != null && soft) {
            string += "soft";
        }

        return string;
    }

    public String transcription() {
        String phonemeString;
        if ((stunned != null && stunned) || (voiced != null && !voiced && stunning != null)) {
            phonemeString = stunning;
        } else {
            phonemeString = translation;
        }

        if (vowel) {
            if (accentType != null) {
                phonemeString += accentType.value;
            } else {
                phonemeString += AccentType.NOT_ACCENT.value;
            }

            if (doubled != null && doubled) {
                phonemeString = "J'0," + phonemeString;
            }

        } else {
            if (soft != null && soft) {
                phonemeString += '\'';
            }

            if (doubled != null && doubled) {
                phonemeString += 1;
            } else {
                phonemeString += 0;
            }
        }

        return phonemeString;
    }

    public enum AccentType {
        ACCENT("0"),
        SUB_ACCENT("1"),
        NOT_ACCENT("2");

        public final String value;

        AccentType(String value) {
            this.value = value;
        }
    }

    // Program property
    private AccentType accentType;
    private Boolean last;
    private Boolean stunned;

    // JSON phonemes properties
    private String name;
    private Boolean vowel;
    private Boolean soft;
    private Boolean voiced;
    private Boolean doubled;
    private String stunning;
    private String translation;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isVowel() {
        return vowel;
    }

    public void setVowel(boolean vowel) {
        this.vowel = vowel;
    }

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    public boolean isSoft() {
        if(soft == null) return false;
        return soft;
    }

    public void setSoft(boolean soft) {
        this.soft = soft;
    }

    public String getStunning() {
        return stunning;
    }

    public void setStunning(String stunning) {
        this.stunning = stunning;
    }

    public boolean isVoiced() {
        if(voiced == null) return false;
        return voiced;
    }

    public void setVoiced(boolean voiced) {
        this.voiced = voiced;
    }

    public boolean isDoubled() {
        if(doubled == null) return false;
        return doubled;
    }

    public void setDoubled(boolean doubled) {
        this.doubled = doubled;
    }

    public AccentType getAccentType() {
        return accentType;
    }

    public void setAccentType(AccentType accentType) {
        this.accentType = accentType;
    }

    public Boolean isLast() {
        return last;
    }

    public void setLast(Boolean last) {
        this.last = last;
    }

    public Boolean isStunned() {
        return stunned;
    }

    public void setStunned(Boolean stunned) {
        this.stunned = stunned;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Phoneme)) {
            return false;
        }

        Phoneme phoneme = (Phoneme) o;

        if (accentType != phoneme.accentType) {
            return false;
        }
        if (name != null ? !name.equals(phoneme.name) : phoneme.name != null) {
            return false;
        }
        if (vowel != null ? !vowel.equals(phoneme.vowel) : phoneme.vowel != null) {
            return false;
        }
        if (soft != null ? !soft.equals(phoneme.soft) : phoneme.soft != null) {
            return false;
        }
        if (voiced != null ? !voiced.equals(phoneme.voiced) : phoneme.voiced != null) {
            return false;
        }
        if (doubled != null ? !doubled.equals(phoneme.doubled) : phoneme.doubled != null) {
            return false;
        }
        if (stunning != null ? !stunning.equals(phoneme.stunning) : phoneme.stunning != null) {
            return false;
        }
        return translation != null ? translation.equals(phoneme.translation) : phoneme.translation == null;

    }

    public boolean propEquals(Phoneme phoneme) {

//        if (accentType != phoneme.accentType) {
//            return false;
//        }
//        if (name != null ? !name.equals(phoneme.name) : phoneme.name != null) {
//            return false;
//        }
        if (this.vowel != null && !this.vowel.equals(phoneme.vowel)) {
            return false;
        }
        if (soft != null && !soft.equals(phoneme.soft)) {
            return false;
        }
        if (voiced != null && !voiced.equals(phoneme.voiced)) {
            return false;
        }
        if (doubled != null && !doubled.equals(phoneme.doubled)) {
            return false;
        }
        if (stunned != null && !stunned.equals(phoneme.stunned)) {
            return false;
        }
        return true;

    }


    public void copyParams(Phoneme phoneme) {
        if (phoneme.getAccentType() != null) {
            this.accentType = phoneme.getAccentType();
        }
        if (phoneme.vowel != null) {
            this.vowel = phoneme.isVowel();
        }
        if (phoneme.soft != null) {
            this.soft = phoneme.isSoft();
        }
        if (phoneme.voiced != null) {
            this.voiced = phoneme.isVoiced();
        }
        if (phoneme.doubled != null) {
            this.doubled = phoneme.isDoubled();
        }
        if (phoneme.stunned != null) {
            this.stunned = phoneme.stunned;
        }
    }

    @Override
    public int hashCode() {
        int result = accentType != null ? accentType.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (vowel != null ? vowel.hashCode() : 0);
        result = 31 * result + (soft != null ? soft.hashCode() : 0);
        result = 31 * result + (voiced != null ? voiced.hashCode() : 0);
        result = 31 * result + (doubled != null ? doubled.hashCode() : 0);
        result = 31 * result + (stunning != null ? stunning.hashCode() : 0);
        result = 31 * result + (translation != null ? translation.hashCode() : 0);
        return result;
    }
}
