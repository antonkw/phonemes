package by.bsuir.processor;

import by.bsuir.model.Phoneme;
import by.bsuir.util.JsonUtil;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class BYProcessor implements WordProcessor {

    @Override
    public String process(String word, Map<List<Phoneme>, List<Phoneme>> rules) {
        List<Phoneme> result = convertWordToPhonemes(word.replace("-", ""));
        result = applyRules(result, rules);
        return makeWordTranscription(result);
    }

    private List<Phoneme> convertWordToPhonemes(String word) {
        List<Phoneme> phonemes = new ArrayList<>();
        for (int i = 0; i < word.length(); i++) {
            Phoneme phoneme = null;
            if (i + 2 < word.length()) {
                String chars = word.substring(i, i + 3);
                phoneme = JsonUtil.getPhoneme(chars);
                if (phoneme != null) {
                    phoneme.setDoubled(true);
                    i += 2;
                }
            }
            if (phoneme == null && i + 1 < word.length()) {
                if (word.charAt(i + 1) == '+') {
                    phoneme = JsonUtil.getPhoneme(String.valueOf(word.charAt(i)));
                    phoneme.setAccentType(Phoneme.AccentType.ACCENT);
                    i++;
                } else if (word.charAt(i + 1) == '=') {
                    phoneme = JsonUtil.getPhoneme(String.valueOf(word.charAt(i)));
                    phoneme.setAccentType(Phoneme.AccentType.SUB_ACCENT);
                    i++;
                } else if (word.charAt(i) == word.charAt(i + 1)) {
                    phoneme = JsonUtil.getPhoneme(String.valueOf(word.charAt(i)));
                    phoneme.setDoubled(true);
                    i++;
                } else {
                    phoneme = JsonUtil.getPhoneme(word.substring(i, i + 2));
                    if (phoneme != null) {
                        i++;
                    }
                }
            }
            if (phoneme == null) {
                phoneme = JsonUtil.getPhoneme(String.valueOf(word.charAt(i)));
                if (phoneme.isVowel()) {
                    phoneme.setAccentType(Phoneme.AccentType.NOT_ACCENT);
                }
            }
            phonemes.add(phoneme);
        }
        return phonemes;
    }

    private List<Phoneme> applyRules(List<Phoneme> phonemes, Map<List<Phoneme>, List<Phoneme>> rules) {
        for (int i = phonemes.size() - 1; i > 0; i--) {
            Phoneme prevPhoneme = phonemes.get(i - 1);
            Phoneme currentPhoneme = phonemes.get(i);
            List<Phoneme> phonemesToCheck = Arrays.asList(prevPhoneme, currentPhoneme);

            for (Map.Entry<List<Phoneme>, List<Phoneme>> rule : rules.entrySet()) {
                List<Phoneme> conditionOfRule = rule.getKey();
                List<Phoneme> resultOfRule = rule.getValue();
                if (conditionOfRule.size() == 1 && conditionOfRule.get(0).isLast()) {
                    if (i != phonemes.size() - 1) {
                        continue;
                    }
                }
                int sizeToIterate = phonemesToCheck.size() < conditionOfRule.size() ? phonemesToCheck.size() : conditionOfRule.size();
                boolean isRuleApplyable = true;

                for (int m = sizeToIterate - 1; m >= 0; m--) {
                    if (!conditionOfRule.get(m).propEquals(phonemesToCheck.get(m + phonemesToCheck.size() - sizeToIterate))) {
                        isRuleApplyable = false;
                    }
                }

                if (isRuleApplyable) {
                    for (int n = sizeToIterate - 1; n >= 0; n--) {
                        phonemesToCheck.get(n + phonemesToCheck.size() - sizeToIterate).copyParams(resultOfRule.get(n));
                    }
                }
            }
        }




        return phonemes;
    }

    private String makeWordTranscription(List<Phoneme> phonemes) {
//        List<String> transcriptedPhonemes  = phonemes
//                .stream()
//                .filter(phoneme -> !phoneme.getName().equals("ь"))
//                .map(Phoneme::transcription)
//                .collect(Collectors.toList());

        List<String> transcripted = new ArrayList<>();

        for (Phoneme phoneme: phonemes) {
            if (!phoneme.getName().equals("ь")) {
                transcripted.add(phoneme.transcription());
            }
        }

        return String.format("[%s]", StringUtils.join(transcripted, ','));
    }
}
