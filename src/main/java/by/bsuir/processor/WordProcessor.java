package by.bsuir.processor;

import by.bsuir.model.Phoneme;

import java.util.List;
import java.util.Map;

public interface WordProcessor {

    String process(String word, Map<List<Phoneme>, List<Phoneme>> rules);
}
