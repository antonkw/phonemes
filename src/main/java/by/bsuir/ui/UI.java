package by.bsuir.ui;


import by.bsuir.PhonemicTranscript;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;


public class UI extends Application {
    private Scene scene;
    private Label sourceLabel = new Label("Word:");
    private TextField source = new TextField();
    private Button word = new Button("Process word");
    private Button test = new Button("Choose file");
    private Button clear = new Button("Clear");
    private TextArea textArea = new TextArea();

    private HBox words = new HBox();
    private HBox file = new HBox();
    private VBox all = new VBox();
    private VBox tools = new VBox();
    private HBox textarea = new HBox();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(final Stage primaryStage) throws Exception {
        primaryStage.setTitle("Phoneme Processor");
        final BorderPane root = new BorderPane();
        scene = new Scene(root, 800, 800);
        primaryStage.setScene(scene);
        primaryStage.show();
        words.setSpacing(5);
        file.setSpacing(5);
        tools.setSpacing(5);
        words.setAlignment(Pos.CENTER);
        file.setAlignment(Pos.CENTER);
        tools.setAlignment(Pos.CENTER);
        all.setAlignment(Pos.CENTER);
        textarea.setAlignment(Pos.CENTER);

        words.getChildren().addAll(sourceLabel, source, word);
        file.getChildren().addAll(test, clear);
        tools.getChildren().addAll(words, file);
        textarea.getChildren().addAll(textArea);
        textarea.setMinHeight(700);
        textarea.setMinWidth(720);
        textArea.setMinWidth(720);
        textArea.setWrapText(true);
        all.getChildren().addAll(tools, textarea);
        root.setTop(all);
        root.setBottom(textarea);
        root.setPadding(new Insets(10, 10, 10, 10));


        test.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Open Resource File");
                File file = fileChooser.showOpenDialog(primaryStage);
                if (file != null) {
                    PhonemicTranscript.process("file", file.getAbsolutePath(), new Printer(textArea));
                }

            }
        });

        word.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                PhonemicTranscript.process("word", source.getText(), new Printer(textArea));
            }
        });

        clear.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                textArea.clear();
            }
        });
    }
}
