package by.bsuir.ui;


import javafx.scene.control.TextArea;

public class Printer {

    private TextArea textArea;

    public Printer(TextArea textArea) {
        this.textArea = textArea;
    }

    public void print(String text){
        textArea.appendText(text);
    }

    public void clear(){
        textArea.setText("");
    }
}
