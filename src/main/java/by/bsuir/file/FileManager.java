package by.bsuir.file;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class FileManager {

    public static final String DEFAULT_FILE_ENCODING = "windows-1251";

    private String path;
    private String encoding;

    public FileManager(String path) {
        this.path = path;
        this.encoding = DEFAULT_FILE_ENCODING;
    }

    public FileManager(String path, String encoding) {
        this(path);
        this.encoding = encoding;
    }

    public Map<String, String> processFile() throws IOException {
        Map<String,String> testData;

        try(BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(path), encoding))) {
            testData = new HashMap<>();

            String line;
            String word = null;
            String transcription = null;
            while ((line = reader.readLine()) != null) {
                if(word != null && transcription != null) {
                    testData.put(word, transcription);
                    word = null;
                    transcription = null;
                }
                line = line.trim();
                if(!line.isEmpty()) {
                    if(line.startsWith("[")) {
                        transcription = line;
                    }
                    else {
                        word = line.replaceAll("\\.", "").toLowerCase();
                    }
                }
            }
        }
        return testData;
    }


}
