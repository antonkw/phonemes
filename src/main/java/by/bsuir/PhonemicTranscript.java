package by.bsuir;

import by.bsuir.file.FileManager;
import by.bsuir.model.Phoneme;
import by.bsuir.processor.BYProcessor;
import by.bsuir.processor.WordProcessor;
import by.bsuir.ui.Printer;
import by.bsuir.util.JsonUtil;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Hello world!
 */
public class PhonemicTranscript {
    public static final String FLAG_WORD_MODE = "word";
    public static final String FLAG_FILE_MODE = "file";

    private static WordProcessor processor;
    /*
        FIRST PARAM: flag - 'word'/'file'
        SECOND PARAM: word/path to file
     */

//    public static void main(String[] args)  {
//        System.out.println("FIRST PARAM: flag - 'word'/'file' \nSECOND PARAM: word/path to file");
//        String mode ="",  src ="";
//        Scanner sc = new Scanner(System.in);
//        System.out.println("input mode and source: ");
//        if(sc.hasNextLine()) {
//            mode =  sc.nextLine();
//        }
//        if(sc.hasNextLine()) {
//            src =  sc.nextLine();
//        }
//        process(mode, src);
//
//
//        System.out.println("\n Press Enter to continue...");
//
//        try {
//            System.in.read();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        System.out.println("Finished.");
//    }


    public static void main(String[] args) {
        String systemFlag = args[0];
        String source = args[1];
        process(systemFlag, source);
    }

    public static void process(String mode, String src) {

        if (!(mode.equalsIgnoreCase(FLAG_WORD_MODE) || mode.equalsIgnoreCase(FLAG_FILE_MODE))) {
            System.out.println("Invalid transcription parameters !");
            System.out.println("Please try again.");
        } else {
            switch (mode) {
                case FLAG_WORD_MODE:
                    processWordTranscription(src);
                    break;

                case FLAG_FILE_MODE:
                    processFileTranscription(src);
                    break;
            }
        }
    }


    public static void process(String mode, String src, Printer printer) {

        if (!(mode.equalsIgnoreCase(FLAG_WORD_MODE) || mode.equalsIgnoreCase(FLAG_FILE_MODE))) {
            System.out.println("Invalid transcription parameters !");
            System.out.println("Please try again.");
        } else {
            switch (mode) {
                case FLAG_WORD_MODE:
                    processWordTranscription(src, printer);
                    break;

                case FLAG_FILE_MODE:
                    processFileTranscription(src, printer);
                    break;
            }
        }
    }

    public static void processFileTranscription(String filePath) {
        FileManager fManager = new FileManager(filePath);
        BufferedWriter writer = null;
        try {
            String timeLog = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(Calendar.getInstance().getTime());
            File logFile = new File("result by " + timeLog + ".txt");

            // This will output the full path where the file will be written to...


            writer = new BufferedWriter(new FileWriter(logFile));
            Map<String, String> testData = fManager.processFile();
            Map<List<Phoneme>, List<Phoneme>> rules = JsonUtil.getRules();

            processor = new BYProcessor();

            Set<Map.Entry<String, String>> entries = testData.entrySet();
            int successCounter = 0;
            int failCounter = 0;

            List<String> success = new ArrayList<>();
            List<String> fail = new ArrayList<>();


            for (Map.Entry<String, String> entry : entries) {
                String transcription = processor.process(entry.getKey(), rules);
                if (transcription.equals(entry.getValue())) {
                    success.add(String.format("Word '%s' transcription: '%s'. Success!", entry.getKey(), transcription));
                    successCounter++;
                } else {
                    fail.add(String.format("Word '%s' transcription: '%s'. Should be: '%s'. Fail!", entry.getKey(), transcription, entry.getValue()));
                    failCounter++;
                }
            }

            writer.write("\n\n==================================================================================\n");
            System.out.print("\n\n==================================================================================\n");
            writer.write("================================SUCCESS===========================================\n");
            System.out.print("================================SUCCESS===========================================\n");
            writer.write("==================================================================================\n");
            System.out.print("==================================================================================\n");

            for (String s : success) {
                System.out.println(s);
                writer.write(s + "\n");
            }


            if (fail.size() == 0) {
                writer.write("\n================================NO FAILS==========================================\n");
                System.out.print("\n================================NO FAILS==========================================\n");
            } else {
                writer.write("\n==================================================================================\n");
                writer.write("==================================FAIL============================================\n");
                writer.write("==================================================================================\n");
                System.out.print("\n==================================================================================\n");
                System.out.print("==================================FAIL============================================\n");
                System.out.print("==================================================================================\n");
            }

            for (String s : fail) {
                System.out.println(s);
                writer.write(s + "\n");
            }


            System.out.print("\n\n================================OVERALL==========================================\n");
            writer.write("\n\n================================OVERALL==========================================\n");


            writer.write(String.format("SUCCESS: %d; FAIL: %d", successCounter, failCounter));
            System.out.println(String.format("                         SUCCESS: %d; FAIL: %d", successCounter, failCounter));
            System.out.println("Result saved to " + logFile.getCanonicalPath());


            //create a temporary file


        } catch (Exception e) {
            System.out.println("The file path you are specified is not exist!");
        } finally {
            try {
                // Close the writer regardless of what happens...
                writer.close();
            } catch (Exception e) {
            }
        }

    }

    public static void processWordTranscription(String word) {
        processor = new BYProcessor();
        String result = processor.process(word, JsonUtil.getRules());
        System.out.printf("Result of processing '%s': %s", word, result);
    }

    public static void processFileTranscription(String filePath, Printer printer) {
        FileManager fManager = new FileManager(filePath);
        BufferedWriter writer = null;
        try {
            String timeLog = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(Calendar.getInstance().getTime());
            File logFile = new File("result by " + timeLog + ".txt");

            // This will output the full path where the file will be written to...


            writer = new BufferedWriter(new FileWriter(logFile));
            Map<String, String> testData = fManager.processFile();
            Map<List<Phoneme>, List<Phoneme>> rules = JsonUtil.getRules();

            processor = new BYProcessor();

            Set<Map.Entry<String, String>> entries = testData.entrySet();
            int successCounter = 0;
            int failCounter = 0;

            List<String> success = new ArrayList<>();
            List<String> fail = new ArrayList<>();


            for (Map.Entry<String, String> entry : entries) {
                String transcription = processor.process(entry.getKey(), rules);
                if (transcription.equals(entry.getValue())) {
                    success.add(String.format("Word '%s' transcription: '%s'. Success!", entry.getKey(), transcription));
                    successCounter++;
                } else {
                    fail.add(String.format("Word '%s' transcription: '%s'. Should be: '%s'. Fail!", entry.getKey(), transcription, entry.getValue()));
                    failCounter++;
                }
            }

            writer.write("\n\n================================================================\n");
            printer.print("\n\n================================================================\n");
            writer.write("========================SUCCESS=================================\n");
            printer.print("========================SUCCESS=================================\n");
            writer.write("================================================================\n");
            printer.print("================================================================\n");

            for (String s : success) {
                printer.print(s+ "\n");
                writer.write(s + "\n");
            }


            if (fail.size() == 0) {
                writer.write("\n========================NO FAILS==============================\n");
                printer.print("\n========================NO FAILS==============================\n");
            } else {
                writer.write("\n================================================================\n");
                writer.write("==========================FAIL==================================\n");
                writer.write("================================================================\n");
                printer.print("\n================================================================\n");
                printer.print("==========================FAIL==================================\n");
                printer.print("================================================================\n");
            }

            for (String s : fail) {
                System.out.println(s);
                writer.write(s + "\n");
                printer.print(s + "\n");
            }


            printer.print("\n\n=======================OVERALL================================\n");
            writer.write("\n\n=======================OVERALL================================\n");


            writer.write(String.format("SUCCESS: %d; FAIL: %d", successCounter, failCounter));
            printer.print(String.format("                         SUCCESS: %d; FAIL: %d", successCounter, failCounter));
            printer.print("\nResult saved to " + logFile.getCanonicalPath());


            //create a temporary file


        } catch (Exception e) {
            printer.print("The file path you are specified is not exist!");
        } finally {
            try {
                // Close the writer regardless of what happens...
                writer.close();
            } catch (Exception e) {
            }
        }

    }

    public static void processWordTranscription(String word, Printer printer) {
        processor = new BYProcessor();
        String result = processor.process(word, JsonUtil.getRules());
        printer.print(String.format("Result of processing '%s': %s\n", word, result));
    }
}