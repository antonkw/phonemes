package by.bsuir.util;

import by.bsuir.model.Phoneme;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.ReadContext;
import com.jayway.jsonpath.TypeRef;
import com.jayway.jsonpath.spi.json.JacksonJsonProvider;
import com.jayway.jsonpath.spi.mapper.JacksonMappingProvider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class JsonUtil {

    private static ReadContext json;

    static {
        try(BufferedReader reader = new BufferedReader(
                new InputStreamReader(JsonUtil.class.getResourceAsStream("/rules.json"), "UTF-8"))) {
            StringBuffer buffer = new StringBuffer();
            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }

            Configuration conf = Configuration
                    .builder()
                    .mappingProvider(new JacksonMappingProvider())
                    .jsonProvider(new JacksonJsonProvider())
                    .build();
            json = JsonPath
                    .using(conf)
                    .parse(buffer.toString());
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Phoneme getPhoneme(String letter) {
        List<Phoneme> result = json.read("$.phonemes[?(@.name=='" + letter + "')]", new TypeRef<List<Phoneme>>(){});
        if(!result.isEmpty()) {
            return result.get(0);
        }
        return null;
    }

    public static Map<List<Phoneme>, List<Phoneme>> getRules() {
        Map<List<Phoneme>, List<Phoneme>> rules = new HashMap<>();

        String[] ruleStrings = json.read("$.rules", String[].class);
        for(String rule : ruleStrings) {
            String[] parts = rule.split("->");
            List<Phoneme> before = convertRulePartToPhonemes(parts[0]);
            List<Phoneme> after = convertRulePartToPhonemes(parts[1]);
            rules.put(before, after);
        }
        return rules;
    }

    private static List<Phoneme> convertRulePartToPhonemes(String rulePart) {
        List<Phoneme> phonemes = new ArrayList<>();

        String[] rulePhonemes = rulePart.split(",");
        for(String rulePhoneme : rulePhonemes) {
            Phoneme phoneme = new Phoneme();
            rulePhoneme = rulePhoneme.replaceAll("[\\[\\]]", "");
            if(rulePhoneme.charAt(0) == '$') {
                phoneme.setLast(true);
            }
            for(int i = 0; i < rulePhoneme.length(); i++) {
                char rp = rulePhoneme.charAt(i);
                boolean inverse = false;
                if(rp == '!') {
                    rp = rulePhoneme.charAt(++i);
                    inverse = true;
                }
                if(rp == 'C') phoneme.setVowel(false);
                if(rp == 'V') phoneme.setVowel(true);
                if(rp == 'c') phoneme.setDoubled(!inverse);
                if(rp == 's') phoneme.setSoft(!inverse);
                if(rp == 'v') phoneme.setVoiced(!inverse);
            }
            phonemes.add(phoneme);
        }
        return Collections.unmodifiableList(phonemes);
    }
}
