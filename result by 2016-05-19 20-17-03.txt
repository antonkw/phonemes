

==================================================================================
================================SUCCESS===========================================
==================================================================================
Word 'тры+' transcription: '[T0,R0,Y0]'. Success!
Word 'до+м' transcription: '[D0,O0,M0]'. Success!
Word 'за+яц' transcription: '[Z0,A0,J'0,A2,C0]'. Success!
Word 'жу+к' transcription: '[ZH0,U0,K0]'. Success!
Word 'сястра+' transcription: '[S'0,A2,S0,T0,R0,A0]'. Success!
Word 'со+нейка' transcription: '[S0,O0,N'0,E2,J'0,K0,A2]'. Success!
Word 'шэ=ра-чо+рны' transcription: '[SH0,E1,R0,A2,CH0,O0,R0,N0,Y2]'. Success!
Word 'ве+цце' transcription: '[V'0,E0,C'1,E2]'. Success!
Word 'пя+ць' transcription: '[P'0,A0,C'0]'. Success!
Word 'паву+к' transcription: '[P0,A2,V0,U0,K0]'. Success!
Word 'шэрава+ты' transcription: '[SH0,E2,R0,A2,V0,A0,T0,Y2]'. Success!
Word 'бра+т' transcription: '[B0,R0,A0,T0]'. Success!
Word 'гру+ша' transcription: '[GH0,R0,U0,SH0,A2]'. Success!
Word 'тро=хго+ддзе' transcription: '[T0,R0,O1,H0,GH0,O0,DZ'1,E2]'. Success!

================================NO FAILS==========================================


================================OVERALL==========================================
SUCCESS: 59; FAIL: 0